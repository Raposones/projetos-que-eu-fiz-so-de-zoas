package br.ufc.quixada.cc.model;

import java.util.Scanner;

public class Empresa_Servico extends Empresa {
	
	static Scanner scan = new Scanner(System.in); 
	
	public Empresa_Servico() {
		super();
	}
		 
	public Empresa_Servico(String id, String nome, double valorUnitario) {
		super(id, nome, valorUnitario);
	}

	
	public void adicionar() {

	}
	
	public void Dados() {

		System.out.println("NOME DO SERVI�O: ");
		setNome(scan.nextLine());
		
		System.out.println("ID DO SERVI�O: ");
		setId(scan.nextLine());
		
		System.out.println("VALOR DO SERVI�O ");
		setValorUnitario(Double.parseDouble(scan.nextLine()));
	
	}
	
	public String toString() {
		return "SERVI�O\n #ID: " +getId() + "\nServico: " +getNome() + "\nValor: " +getValorUnitario() + "\n\n";
	}
}
