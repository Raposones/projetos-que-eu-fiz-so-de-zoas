package br.ufc.quixada.cc.model;


public class Orcamento {

	private int id;
	private Cliente c1;
	private double total;
	
	public Orcamento() {
		
	}

	public Orcamento(int id, Cliente c1, double total) {
		super();
		this.id = id;
		this.c1 = c1;
		this.total = total;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Cliente getC1() {
		return c1;
	}

	public void setC1(Cliente c1) {
		this.c1 = c1;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "\n***Orcamento do cliente " +this.c1.getNome() +"\n\n"
				+ "DADOS DO CLIENTE:\n " +this.c1;
	}
	
	
	
}
