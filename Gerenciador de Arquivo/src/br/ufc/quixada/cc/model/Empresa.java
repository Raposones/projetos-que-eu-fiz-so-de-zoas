package br.ufc.quixada.cc.model;

public abstract class Empresa {

	
	private String id;
    private String nome;
    private double valorUnitario;

    

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public Empresa() {

    }
    
    public Empresa(String id,String nome,double valorUnitario) {
        this.id = id;
        this.nome =nome;
        this.valorUnitario = valorUnitario;
    }

    public void adicionar() {
    }
    public void remover() {

    }
    
    public abstract void Dados();
    
}
