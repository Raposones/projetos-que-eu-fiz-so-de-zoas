package br.ufc.quixada.cc.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Cliente {
	
	static Scanner scan = new Scanner(System.in); 
	
	private String nome;
	private String cpf;
	private String endereco;
	private String telefone;
	private String email;
	Map<String, Empresa> listaDeCompras;
	Set<String> keys;
	
	public Set<String> getKeys() {
		return keys;
	}

	public void setKeys(Set<String> keys) {
		this.keys = keys;
	}

	public Cliente() {
		this.setListaDeCompras( new HashMap<String, Empresa>() );
	}
	
	public Cliente(String nome, String cpf, String endereco,  String telefone, String email, Map<String, Empresa> listaDeCompras) {
		this.nome = nome;
		this.cpf = cpf;
		this.endereco = endereco;
		this.telefone = telefone;
		this.setListaDeCompras(listaDeCompras);
	}
	
	public String getEndereco() {
		return endereco;
	}
	
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public void Dados() {
		System.out.println("Qual seu nome? ");
		setNome(scan.nextLine());
		
		System.out.println("CPF? ");
		setCpf(scan.nextLine());
		
		System.out.println("Endereco?");
		setEndereco(scan.nextLine());
		
		System.out.println("Telefone?");
		setTelefone(scan.nextLine());
		
		System.out.println("Email?");
		setEmail(scan.nextLine());
		
		
	}
	

	public Map<String, Empresa> getListaDeCompras() {
		return listaDeCompras;
	}

	public void setListaDeCompras(Map<String, Empresa> listaDeCompras) {
		this.listaDeCompras = listaDeCompras;
		this.keys  = listaDeCompras.keySet();
	}
	
	public String toString() {
		return "Cliente: " + this.nome + "\nCPF: " + this.cpf + "\nEndereco: " + this.endereco + "\nTelefone: " 
				+ this.telefone + "\nEmail: " + this.email +"\n\n";
	}
	
	
	
}
