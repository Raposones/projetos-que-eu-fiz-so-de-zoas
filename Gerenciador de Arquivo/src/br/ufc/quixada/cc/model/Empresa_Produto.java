package br.ufc.quixada.cc.model;
import java.util.Scanner;

public class Empresa_Produto extends Empresa {
	
	static Scanner scan = new Scanner(System.in); 
	
	public Empresa_Produto() {
		super();
	}
		 
	public Empresa_Produto(String id, String nome, double valorUnitario) {
		super(id, nome, valorUnitario);
	}
	
	public void Dados() {
		System.out.println("NOME DO PRODUTO: ");
		setNome(scan.nextLine());
		
		System.out.println("ID DO PRODUTO: ");
		setId(scan.nextLine());
		
		System.out.println("VALOR DO PRODUTO ");
		setValorUnitario(Double.parseDouble(scan.nextLine()));
	}
	
	public String toString() {
		return "PRODUTO \n#ID: " +getId() + "\nProduto: " +getNome() + "\nValor: " +getValorUnitario() + "\n\n";
	}
	
}

