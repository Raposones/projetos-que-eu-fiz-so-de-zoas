package br.ufc.quixada.cc.exec;

import java.time.LocalDate;
import br.ufc.quixada.cc.model.*;



public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		LocalDate dataNasci = LocalDate.of(2000, 7, 6);
		LocalDate promo = LocalDate.of(2018, 3, 22);
		LocalDate admis = LocalDate.of(2018, 1, 31);
		
		Aluno aluno = new Aluno("2345678sla", "Raphael", "2378462374-12", dataNasci);
		ChefeDepartamento chefe = new ChefeDepartamento("SLA.LMTD", promo, 50, "2345678sla", admis, 1000, "Raphael", "2378462374-12", dataNasci);
		
		//chefe.mostrarPessoa();
		chefe.mostrarFuncionario();
		chefe.mostrarChefe();
		
		//aluno.mostrarPessoa();
		aluno.mostrarAluno();
		

		
		
	}

}
