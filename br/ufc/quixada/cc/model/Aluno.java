package br.ufc.quixada.cc.model;

import java.time.LocalDate;

public class Aluno extends Pessoa {
	
	private String matricula;



public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

public Aluno(String matricula, String nome, String cpf, LocalDate dataNasc) {
	super(nome, cpf, dataNasc);
	this.matricula = matricula;
}


public void mostrarAluno() {
	System.out.println("---DADOS DO ALUNO---\n\nNome: " +this.getNome() +"\nCPF: " +this.getCpf() + "\nData de nascimento: " +this.getDataNasc());
	System.out.println("\nMatricula: " +this.getMatricula() +"\n\n");
}

}