package br.ufc.quixada.cc.model;

import java.time.LocalDate;
import java.util.Scanner;

public class Funcionario extends Pessoa {
	
	Scanner scan = new Scanner(System.in);
	
	private String matricula;
	private LocalDate dataAdmissao;
	private double salario;



public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		
		this.matricula = matricula;
	}

	public LocalDate getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(LocalDate dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	
public Funcionario(String matricula, LocalDate dataAdmissao, double salario, String nome, String cpf, LocalDate dataNasc) {
	super(nome, cpf, dataNasc);
	this.matricula = matricula;
	this.dataAdmissao = dataAdmissao;
	this.salario = salario;
}

public void mostrarFuncionario()
{
	System.out.println("---DADOS DO FUNCIONÁRIO---\n\nNome: " +this.getNome() + "\nCPF: " +this.getCpf() +"\nData de nascimento: " +this.getDataNasc());
	System.out.println("\nMatricula: " +this.getMatricula() +"\nData de admissão: " +this.getDataAdmissao() + "\nSalario: " +this.getSalario() +"\n\n");
}
}