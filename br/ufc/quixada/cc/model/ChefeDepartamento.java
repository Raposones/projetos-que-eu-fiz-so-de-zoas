package br.ufc.quixada.cc.model;

import java.time.LocalDate;


public class ChefeDepartamento extends Funcionario {
	
	private String departamento;
	private LocalDate dataPromocao;
	private double gratificacao;



public String getDepartamento() {
		return departamento;
	}



	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}



	public LocalDate getDataPromocao() {
		return dataPromocao;
	}



	public void setDataPromocao(LocalDate dataPromocao) {
		this.dataPromocao = dataPromocao;
	}



	public double getGratificacao() {
		return gratificacao;
	}



	public void setGratificacao(double gratificacao) {
		this.gratificacao = gratificacao;
	}



public ChefeDepartamento(String departamento, LocalDate dataPromocao, double gratificacao, String matricula, LocalDate dataAdmissao, double salario, String nome, String cpf, LocalDate dataNasc) {
	super(matricula, dataAdmissao, salario, nome, cpf, dataNasc);
	this.departamento = departamento;
	this.dataPromocao = dataPromocao;
	this.gratificacao = gratificacao;
			
	
}

public void mostrarChefe() {
	System.out.println("---DEPARTAMENTO---\n\nDepartamento: " +this.getDepartamento() +"\nData da promoção do fucionário: " +this.getDataPromocao() +"\nGratificação: " +this.getGratificacao());
}

}
