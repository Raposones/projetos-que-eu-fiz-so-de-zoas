package br.ufc.quixada.cc.model;

import java.time.LocalDate;


public class Pessoa {
	
	private String nome;
	private String cpf;
	private LocalDate dataNasc;



public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public LocalDate getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(LocalDate dataNasc) {
		this.dataNasc = dataNasc;
	}
	

public Pessoa(String nome, String cpf, LocalDate dataNasc) {
	this.nome = nome;
	this.cpf = cpf;
	this.dataNasc = dataNasc;
}

public void mostrarPessoa(){
	
	System.out.println("----DADOS DA PESSOA----\n\nNome: " +this.getNome() + "\nCPF: " + this.getCpf() + "\nData de nascimento: " + this.getDataNasc() +"\n\n");
}

}